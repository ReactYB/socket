import React, { useEffect, useState } from "react";
import "./style/style.css";
import SendIcon from "@material-ui/icons/Send";
import io from "socket.io-client";

const publicIp = require("public-ip");
const socket = io("http://192.168.1.150:5000");

const Chat = ({Name}) => {
  const [sms, setSms] = useState([]);
  const [message, setMessage] = useState();
  const [ip, setIP] = useState("Proble ipv");

    socket.on("messageAll", (message) => {
      fetch("http://192.168.1.150:5000/messages")
        .then((e) => e.json())
        .then((e) => setSms(e));
    });

  useEffect(() => {
    const getIP = async () => {
      setIP(await publicIp.v4());
    };
    getIP();

    fetch("http://192.168.1.150:5000/messages")
      .then((e) => e.json())
      .then((e) => setSms(e));
  }, []);

  const envoyerMessage = () => {
    console.log("envoyer !");
    document.querySelector(".inputMessage").value = "";
    socket.emit("messageClient", {
      id: socket.id,
      name:Name,
      Temps: Date.now(),
      msg: message,
      ip: ip,
    });
    fetch("http://192.168.1.150:5000/messages")
      .then((e) => e.json())
      .then((e) => setSms(e));
  };

  const Enter = (e) => {
    if (e.keyCode === 13) envoyerMessage();
  };

  return (
    <div className="divGlobale">
      <div className="divReverse">
        {sms ? (
          <>
            {sms.map((e) => {
              return (
                <div
                  className={
                    socket.id === e.socket_id ? "Users divUser" : "Users"
                  }
                  key={e.Temps}
                >
                  <strong
                    style={
                      socket.id === e.socket_id
                        ? { color: "#3a5099" }
                        : { color: "#216939" }
                    }
                  >
                    {e.name}
                  </strong>{" "}
                  <span
                    className={
                      socket.id === e.socket_id ? "TextRight" : "TextLeft"
                    }
                  >
                    {e.message}{" "}
                  </span>
                  <span
                    className={
                      socket.id === e.socket_id ? "TempsRight" : "TempsLeft"
                    }
                  >
                    {new Date(Number(e.dateMsg)).toString().substring(16, 21)}
                  </span>
                </div>
              );
            })}
          </>
        ) : (
          ""
        )}
      </div>
      <div className="input-container">
        <input
          onKeyUp={(e) => Enter(e)}
          className="inputMessage"
          type="text"
          onChange={(e) => setMessage(e.target.value)}
          placeholder="  Ecrivez votre message ..."
          style={{ backgroundColor: "#ffff" }}
        />
        <button className="btnMessage" onClick={() => envoyerMessage()}>
          <SendIcon style={{ color: "green" }}></SendIcon>
        </button>
      </div>
    </div>
  );
};

export default Chat;
