import React, { useState } from "react";
import Chat from "./Chat";

export const Name = () => {
  const [name, setName] = useState();
  const [X, setX] = useState(false);

  const push = () => {
    setX(true);
  };
  return (
    <div>
      {!X ? (
        <>
          <input
            type="text"
            required
            onChange={(e) => setName(e.target.value)}
          />
          <button onClick={() => push()}>Go !</button>{" "}
        </>
      ) : (
        ""
      )}
      {X ? <Chat Name={name} /> : ""}
    </div>
  );
};
