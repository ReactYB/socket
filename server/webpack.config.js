const path = require("path");

module.exports = {
  
  entry: "./index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "api.bundle.js",
  },
  target: "node",
  externals: {
    "socket.io": "socket.io",
    bufferutil: "bufferutil",
    "utf-8-validate": "utf-8-validate",
  },
};
