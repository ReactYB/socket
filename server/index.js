const app = require("express")();
var cors = require("cors");
app.use(cors());
const server = require("http").createServer(app);
const mysql = require("mysql");

const knex = require("knex")({
  client: "mysql",
  connection: {
    host: "localhost",
    user: "root",
    password: "",
    database: "chat_message",
  },
});


const io = require("socket.io")(server, {
  cors: {
    origin: "*",
    credentials: true,
  },
});

app.get("/messages", function (request, result) {
    knex
      .select()
      .table("messages")
      .then((data) => result.send(data))
  });

io.on("connection", (socket) => {
  socket.on("messageClient", (sms) => {
    
    knex("messages")
      .insert({
        message: sms.msg,
        socket_id: sms.id,
        dateMsg: sms.Temps,
        ip: sms.ip,
        name: sms.name,
      })
      .then((e) => console.log("data insert succees"));
      socket.broadcast.emit("messageAll", sms);
  });
});
server.listen(5000, () => console.log("Port: 5000"));
